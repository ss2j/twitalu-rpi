#!/usr/bin/env python3

from twython import Twython, TwythonError
import twitalu_OPCODE_display as OPCODE_display
from hashids import Hashids
import nixie
import re
import time
from time import gmtime, strftime

twitter = Twython('74GqRHU1JaPvrKx6SHQ4w', '3EL0biFCRuJGLf5ttZ3W6T6ovMsd2JoK3H1QM9GE', '1543952911-NFuG7CC8KF4NxdudU5ZNBlZcNmRhoCPJY673Ehk', 'hCkiPne3tBxlRQ4Qr9UKer0Op8tqwH5QZIu6OilPnU')
hashids = Hashids(salt='OUTATIME')
re_cmd = re.compile('@twittithmetic\s*(\d+)\s*([\+]|[\-]|[\/]|[\*]|AND|OR|XOR|ROR|ROL)\s*(\d+)\s*',re.IGNORECASE)

def get_tweets():
	"""
	Retrieves the mentions and time line from twitter and checks for new tweets.
	
	Accepts:
	--------
	* Nothing
	
	Returns:
	--------
	* A dict object of tweets that have not been responded to yet.
	"""
	
	# If starting the system after a long down time blank the last_id file to a single space character
	id_file = open("last_id.txt", "rt")
	last_id = id_file.read()
	id_file.close()
	
	# Initialise the dict object for storing tweets that need processing.
	tweets = {}
	tweet_number = 0
	
	# Grab the current time because we'll need it and it's helpful to know.
	current_time = strftime("%H:%M:%S", gmtime())
	print("***Retrieveing Timelines*** " + current_time)

	if last_id == ' ':
		try:	# Fetches the last 20 tweets on home time-line (i.e. responses that have been sent).
			home_timeline = twitter.get_home_timeline(screen_name='twittithmetic', count='20')
		except TwythonError as e:
			print(e)
		
		try:	# Fetches the last 20 mentions (i.e. things we have to process).
			mentions_timeline = twitter.get_mentions_timeline(screen_name='twittithmetic', count='20')
		except TwythonError as e:
			print(e)
	else:
		
		try:	# Fetches the last 20 tweets on home time-line (i.e. responses that have been sent).
			home_timeline = twitter.get_home_timeline(screen_name='twittithmetic', count='20', since_id = last_id)
		except TwythonError as e:
			print(e)

		try:	# Fetches the last 20 mentions (i.e. things we have to process).
			mentions_timeline = twitter.get_mentions_timeline(screen_name='twittithmetic', count='20', since_id = last_id)
		except TwythonError as e:
			print(e)

	# Compare the retrieved lists to figure out what's new.
	for x in range(0, len(mentions_timeline)):
		print("|_ Looking for job ID: {0}".format(mentions_timeline[x]["id_str"]))
		tweet_flag = 0
		
		for y in range(0, len(home_timeline)):
			#print("checking response ID: {0}".format(home_timeline[y]["in_reply_to_status_id_str"]))
			if home_timeline[y]["in_reply_to_status_id_str"] == mentions_timeline[x]["id_str"]:
				print("|__ ID match found. Job complete: {0}".format(mentions_timeline[x]["id_str"]))
				tweet_flag = 1
		
		if tweet_flag == 0:
			print("|__ No ID match found. Job not complete: {0}".format(mentions_timeline[x]["id_str"]))
			tweets[tweet_number] = mentions_timeline[x]
			tweet_number = tweet_number + 1
			
	print()
	print("***Tweet queue***")
	for z in range(0, len(tweets)):
		print(tweets[z]["text"])
		
	if len(tweets) > 0:
		print()
		print("Storing last job ID: {0}".format(tweets[0]["id"]))
		id_file = open("last_id.txt", "wt")
		id_file.write(tweets[0]["id_str"])
		id_file.close()
	
	return tweets
		
def send_response(tweet_queue, work, final_key):
	"""
	Replies to tweets with the calculated solutions.
	
	Accepts:
	--------
	* A dict object of complete tweets that have been processed.
	* A dict object of the work received and calculated response.
	* The index of the final work item within the previous two objects.
	
	Returns:
	--------
	* Nothing.
	"""

	print("***Processing Responses***")
	if len(work) > 0:
		for i in range(0, int(final_key) + 1):
			print("|_ Job number: {0}".format(i))
			try:
				tweet_author = tweet_queue[i]["user"]
				tweet_author_screen_name = tweet_author["screen_name"]
				tweet_author_name = tweet_author["name"]
				mention_id_hash = hashids.encrypt(tweet_queue[i]["id"])[0:8]
				
				status_update = "@{0} Hello {1}, Your solution is {3} [{2}]".format(tweet_author_screen_name, tweet_author_name, mention_id_hash, work[i]["4"])
				
				print("|__ Mention ID: {0}  Mention ID Hash: {1}".format(tweet_queue[i]["id_str"], mention_id_hash))
				print("|__ Input from: @{0} Content: {1}".format(tweet_author_screen_name, tweet_queue[i]["text"]))	
				print("|___ Response generated: {0}".format(status_update))
				
				time.sleep(3)
				OPCODE_display.display_twit_send()
				time.sleep(5)
				
				try:
					twitter.update_status(in_reply_to_status_id = tweet_queue[i]["id_str"], status = status_update)
				except TwythonError as e:
					print(e)
			except:
				print("|__ No work at this position.")
				print("|___ Conclusion: Work exists later in queue")
			
			OPCODE_display.display_wait()
			time.sleep(5)
			OPCODE_display.countdown(0x11)
			time.sleep(20)	
	else:
		print("|_ Response queue empty.")
		print("|__ Conclusion: No responses")
					
	if len(work) == 0:
		OPCODE_display.display_wait()
		time.sleep(3)
		
def input_scrub(tweets):
	"""
	Uses a reg exp to validate received tweet format and extract work from them
	
	Accepts:
	--------
	* A dict object tweets without responses that may contain work.
	
	Returns:
	--------
	* A dict object of validated tweets.
	* A dict object of extracted work for processing.
	"""
	
	job_number = len(tweets)
	tweet_queue = tweets
	work = {}
	temp = {}
	
	print()
	print("***Scrubbing Tweets***")
	for i in range(0, len(tweets)):
		print("|_ Job number: {0}".format(i))
		cmd_in = tweets[i]["text"]
		try:
			cmd = re_cmd.search(cmd_in)
			print("|__ Group 0: {0}".format( cmd.group(0) ))
			temp["0"] = cmd.group(0)
			print("|__ Group 1: {0}".format( cmd.group(1) ))
			temp["1"] = cmd.group(1)
			print("|__ Group 2: {0}".format( cmd.group(2) ))
			temp["2"] = cmd.group(2)
			print("|__ Group 3: {0}".format( cmd.group(3) ))
			temp["3"] = cmd.group(3)
			temp["4"] = '0'
			temp["5"] = '0'
			work[i] = {}
			work[i].update(temp)
		except:
			print("|__ Reg Ex did not match.")
			print("|___ Conclusion: No valid command")
			del tweet_queue[i]
			print("|____ Action: Job Scrubbed.")
			
	if job_number == 0:
		print("|_ Tweet queue empty.")
		print("|__ Conclusion: No Tweets")

	return (tweet_queue, work)
	
