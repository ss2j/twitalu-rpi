#!/usr/bin/env python3

# Notes:
# Register A data is on Port A of Port Expander A (0x20)
# Register A control is on Port B of Port Expander A
# Output Enable (OE) pins are ACIVE LOW

# Port Expander A:
# I2C address 0x20
#			  	Port B			Port A
#				       _________  
#		Zero Buf OE 0[|	   A 	|]0 A0
#		Port Buf OE 1[|  		|]1 A1
#		Reg A CLK   2[|   MCP	|]2 A2
#				    3[|	 23017 	|]3 A3
#				    4[| 		|]4 A4
#				    5[|			|]5 A5
#				    6[| 		|]6 A6
#				    7[|_________|]7 A7

# Includes
import sys
sys.path.append(r'/home/pi/git/quick2wire-python-api/')
import twitalu_I2C as I2C
import twitalu_globals as globals

# Defines
port_expand_A_addr = 0x20 # I2C address

def init():
	'''Initialises register by loading zeroes into its port expander
	
	Arguments:
		none
		
	Returns:
		none
	'''
	I2C.set_IO_DIR(port_expand_A_addr, 'A', 0x00) 		# set port as output
	I2C.write_data(port_expand_A_addr, 'A', 0x00)		# write all zeros
	I2C.set_IO_DIR(port_expand_A_addr, 'B', 0x00)		# set port as output
	I2C.write_data(port_expand_A_addr, 'B', 0b00000011) # disable both buffers, drive clock low
	clear_register()


def write_port(data):
	'''Writes 8 bits to Port A
	
	Arguments:
		data	8-bit integer
		
	Returns:
		none
	'''
	if globals.twitalu_v01_fixes == False:
		I2C.write_data(port_expand_A_addr, 'A', data)
	elif globals.twitalu_v01_fixes == True:
		# strip the incorrect bits
		bit0 = 0b00000001 & data
		bit1 = 0b00000010 & data
		bit2 = 0b00000100 & data
		bit3 = 0b00001000 & data
		
		# make copies of all bits
		bit0_copy = bit0
		bit1_copy = bit1
		bit2_copy = bit2
		bit3_copy = bit3
		
		# shift incorrect bits so they sit properly
		bit0 = bit1_copy >> 1
		bit1 = bit0_copy << 1
		bit2 = bit3_copy >> 1
		bit3 = bit2_copy << 1
		
		# reconstruct data
		data = data & 0xF0
		data = data | bit0
		data = data | bit1
		data = data | bit2
		data = data | bit3
		
		# write corrected data to port expander
		I2C.write_data(port_expand_A_addr, 'A', data)

def clear_port():
	'''Clears Port A
	
	Arguments:
		none
		
	Returns:
		none
	'''
	I2C.write_data(port_expand_A_addr, 'A', 0x00) # write all zeros
	
def set_port_buffer(enable):
	'''Controls the buffer connected to the Port Expander
	
	Arguments:
		enable (enable = 1, disable = 0)
		
	Returns:
		none
	'''
	old_value = I2C.read_data(port_expand_A_addr, 'B') # read in the previous control pins
	if enable == 1:
		mask = 0b11111101 # set port buffer signal low
		new_value = old_value & mask # AND
	elif enable == 0:
		mask = 0b00000011 # set port buffer signal high
		new_value = old_value | mask # OR
	I2C.write_data(port_expand_A_addr, 'B', new_value) # write to Port B

def set_zero_buffer(enable):
	'''Controls the buffer connected to the zeroes
	
	Arguments:
		enable (enable = 1, disable = 0)
		
	Returns:
		none
	'''
	old_value = I2C.read_data(port_expand_A_addr, 'B') # read in the previous control pins
	if enable == 1:
		mask = 0b11111110 # set zero buffer signal low
		new_value = old_value & mask # AND
	elif enable == 0:
		mask = 0b00000011 # set zero buffer signal high
		new_value = old_value | mask # OR
	I2C.write_data(port_expand_A_addr, 'B', new_value) # write to Port B
	
def clock_data():
	'''Clocks in data to Reg A from the currently selected buffer
	
	Arguments:
		none
		
	Returns:
		none
	'''
	# drive clock high
	old_value = I2C.read_data(port_expand_A_addr, 'B') # read in the previous control pins
	clock_high_mask = 0b00000100
	clock_high_value = old_value | clock_high_mask # OR, set clock high
	I2C.write_data(port_expand_A_addr, 'B', clock_high_value)
	
	# drive clock low
	old_value = I2C.read_data(port_expand_A_addr, 'B') # read in the previous control pins
	clock_low_mask = 0b11111011
	clock_low_value = old_value & clock_low_mask # AND, set clock low
	I2C.write_data(port_expand_A_addr, 'B', clock_low_value)
	
def write_register(data):
	'''Writes an 8-bit word to the register
	
	Arguments:
		data	8-bit integer
		
	Returns:
		none
	'''
	write_port(data)
	set_port_buffer(1)
	clock_data()
	set_port_buffer(0)

def write_zero_register():
	'''Writes  8 zeroes to the register
	
	Arguments:
		data	8-bit integer
		
	Returns:
		none
	'''
	set_zero_buffer(1)
	clock_data()
	set_zero_buffer(0)
	
def clear_register():
	'''Clears the register by writing zeroes to every pin
	
	Arguments:
		none
		
	Returns:
		none
	'''
	clear_port()
	set_port_buffer(1)
	clock_data()
	set_port_buffer(0)
