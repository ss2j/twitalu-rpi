#!/usr/bin/env python3

import twy_twitter as twitter
import twitalu_globals as globals
import twitalu_init as init
import twitalu_OPCODE_display as OPCODE_display
import nixie
import time

from apscheduler.scheduler import Scheduler
from time import localtime, strftime

if globals.no_hardware == True:
	import twitalu_Math_dev as tMath
	nixie.init()
else:
	import twitalu_Math as tMath
	init.init()

global display_status # Initialise the new global variable.
display_status = 0 # Default it to 0.

sched = Scheduler()	# Instantiate the scheduler.

@sched.scheduled_job('interval', {'seconds': 60})
def time_machine():
	"""
	Monitors the time of day and controls the brightness of the displays.
	
	Accepts:
	--------
	* Nothing
	
	Returns:
	--------
	* Nothing
	"""

	global display_status
	day_start = "09:00:00"
	day_dim = "17:00:00"
	day_end = "20:00:00"
	
	current_time = strftime("%H:%M:%S", localtime())
	print("Current time: " + current_time)
	print("Current display status: {0}".format(display_status))

	if current_time >= day_start:
		print("-> The day has begun")
		if current_time < day_end:
			print("|__ The day hasn't ended")
			if current_time < day_dim:
				print("|____ We are before the dimming time")
				print("|______ Displays to full brightness")
				if display_status != 1:
					display_status = 1	# 1: displays full brightness
					nixie.dim_display(70)
					OPCODE_display.sleep_mode_OFF()
			else:
				print("|____ We are after the dimming time")
				print("|______ Displays to reduced brightness")
				if display_status != 2:
					display_status = 2	# 2: displays dimmed
					nixie.dim_display(35)
					# Dim the OPCODE display here
		else:
			print("|__ The day has ended")
			print("|____ Displays need to be turned off")
			if display_status != 0:
				display_status = 0	# 0: displays off
				nixie.blank_display()
				OPCODE_display.sleep_mode_ON()
	else:
		print("-> The day hasn't begun yet")
		print("|__ The displays should still be off")
		if display_status != 0:
			display_status = 0	# 0: displays off
			nixie.blank_display()
			OPCODE_display.sleep_mode_ON() 
		
@sched.scheduled_job('interval', {'minutes': 2})
def main():
	"""
	Is the main synchronous program loop.
	
	Accepts:
	--------
	* Nothing
	
	Returns:
	--------
	* Nothing
	"""

	if display_status != 0:
		nixie.tumble_display()
		OPCODE_display.sleep_mode_OFF()
		time.sleep(1)

	OPCODE_display.display_twit_check()
	time.sleep(4)
	
	tweets = twitter.get_tweets()
	[tweet_queue, work] = twitter.input_scrub(tweets) 
	# 'tweet_queue' when returned will contain only the valid tweets. 
	# 'work' when returned contains the validated commands from the scrubber.
	# It is directly related to 'tweet_queue'. tweet_queue[0] are work[0] the same job,
	# this relationship must be maintained for the responder to work properly.
	
	try:
		work_keys = list(work.keys())
		final_key = work_keys[(len(work)) - 1]
		print()
		print("Current work queue keys: {0}".format(work_keys))
	except:
		final_key = 0
	print("Final Key: {0}".format(final_key))
	
	print()
	print("***Processing Work***")
	if len(work) > 0:
		for i in range(0, int(final_key) + 1):
			print("|_ Job number: {0}".format(i))
			OPCODE_display.display_twitalu()
			nixie.tumble_display()
			time.sleep(2)
			
			try:
				print("|__ Raw work: {0}".format(work[i]["0"]))
				nixie.write_value(int(work[i]["1"]), 1)
				time.sleep(2)
				
				if work[i]["2"] == "+":
					OPCODE_display.display_ADD()
				elif work[i]["2"] == "-":
					OPCODE_display.display_SUB()
				elif work[i]["2"] == "*":
					OPCODE_display.display_MUL()
				elif work[i]["2"] == "/":
					OPCODE_display.display_DIV()
				elif work[i]["2"] == "AND":
					OPCODE_display.display_AND()
				elif work[i]["2"] == "OR":
					OPCODE_display.display_OR()
				elif work[i]["2"] == "XOR":
					OPCODE_display.display_XOR()
				elif work[i]["2"] == "ROR":
					OPCODE_display.display_ROR()
				elif work[i]["2"] == "ROL":
					OPCODE_display.display_ROL()
				time.sleep(2)
				
				nixie.write_value(int(work[i]["3"]), 2)
				time.sleep(2)
				
				# work[i][0] = {the valid string as matched by the reg ex}
				# work[i][1] = {the valid A part}
				# work[i][2] = {the valid operation part}
				# work[i][3] = {the valid B part}
				# work[i][4] = {is empty but will have xxxxx placed here to indicate the solution as a string}
				# work[i][5] = {is empty but will have xxxxx placed here to indicate the solution as an int}
				
				print("|___ Action: Perform calculation")
				result = tMath.calculate(work[i]["1"],work[i]["2"], work[i]["3"])
				print("|____ Result: {0}".format(result))
				print("|_____ Action: Format for insertion")
				result_str = "{0}".format(result)
				print("|______Result: {0}".format(result_str))
				work[i]["4"] = result_str
				work[i]["5"] = result
				nixie.write_value(int(work[i]["5"]), 3)
				time.sleep(5)
			except:
				print("|__ Job empty.")
				print("|___ Conclusion: Job scrubbed")
	else:
		print("|_ Job queue empty.")
		print("|__ Conclusion: No work")
	
	if display_status != 0:
		nixie.tumble_display()

	twitter.send_response(tweet_queue, work, final_key)
	
	# Put hardware back to sleep
	tMath.calculate(0, "+", 0)
	nixie.blank_display()
	time.sleep(0.2)
	OPCODE_display.sleep_mode_ON()

# Run the main program once at script start up
time_machine()
main()

# Start the scheduler	
sched.configure(daemonic=False)
sched.start()
