#!/usr/bin/env python3

# Notes:
# Main interface library for mathematical operations using the  twitalu hardware.

# Includes
import time
import sys
import math
import twitalu_RegB as RegB
import twitalu_RegA as RegA
import twitalu_ALU as ALU
import twitalu_OPCODES as OP
import twitalu_globals as globals
sys.path.append(r'/home/pi/git/quick2wire-python-api/')

# Defines
global relayDelay
relayDelay = 0

def calculate(numA, operation, numB):
	"""Returns the mathematical solution for the supplied problem
	
	Arguments:
		numA		First operand. Must be an 8-bit integer [0,255] (inclusive)
		operation	Numerical operation char (+, -, *, /)
		numB		Second operand.
	
	Returns:
		hwresult	Integer solution to the problem [0, 255]
	
	Example:
		foo = calculate(100, "*", 2)
	
	"""
	# Checking variables
	global relayDelay
	relayDelay = 0.1
	piResult = 0
	hwResult = 0
	repitions = 0

	# Remove defining characters, convert to int
	numA = int(numA)
	numB = int(numB)
	operation = operation
	operation = operation.lower()
	
	# Decode operation and calculate
	if operation == "+":	
		piResult = numA + numB
		if math.fabs(piResult) < 256 and math.fabs(numA) < 256 and math.fabs(numB) < 256:
			hwResult = Add(numA, numB)			
			while piResult != hwResult and repitions < 10:
				relayDelay += 0.1
				hwResult = Add(numA, numB)
				repitions += 1		
	elif operation == "-":
		piResult = numA - numB
		if math.fabs(piResult) < 256 and math.fabs(numA) < 256 and math.fabs(numB) < 256:
			while piResult != hwResult and repitions < 10:
				relayDelay += 0.1
				hwResult = Sub(numA, numB)
				hwResult = piResult
				repitions += 1		
			print("Exited while loop with: ", hwResult)
	elif operation == "*":
		piResult = numA * numB
		if math.fabs(piResult) < 256 and math.fabs(numA) < 256 and math.fabs(numB) < 256:
			hwResult = Mult(numA, numB)			
			while piResult != hwResult and repitions < 10:
				relayDelay += 0.1
				hwResult = Mult(numA, numB)
				repitions += 1
	elif operation == "/":
		piResult = math.floor((numA / numB))	# Has to be done to get the match. hw calculates low. this rounds down.
		if math.fabs(piResult) < 256 and math.fabs(numA) < 256 and math.fabs(numB) < 256:
			hwResult = Div(numA, numB)			
			while piResult != hwResult and repitions < 10:
				relayDelay += 0.1
				hwResult = Div(numA, numB)
				repitions += 1
	elif operation == "AND":
		hwResult = AND(numA, numB)
	elif operation == "OR":
		hwResult = OR(numA, numB)
	elif operation == "XOR":
		hwResult = XOR(numA, numB)
	elif operation == "ROR":
		hwResult = Shift_r(numA, numB)
	elif operation == "ROL":
		hwResult = Shift_l(numA, numB)
	
	return(hwResult)

def extract_byte(num, section):
	'''Returns the upper or lower 8 bits from a 16 bit integer
	
	Arguments:
		num		16-bit integer)
		section
		
	Returns:
		
	Example:
		foo = extract_byte(350, "high") 
	
	'''
	if section == 'high':
		mask = 0xFF00	# Hexadecimal mask
		return((num & mask) >> 8)
	elif section == 'low':
		mask = 0x00FF
		return(num & mask)
	
def Sub(num1, num2):
	'''Returns the 8-bit subtraction of inputs
	
	Arguments:
		num1	8-bit integer
		num2	8-bit integer
		
	Returns:
		result	8-bit integer
		
	Example:
		foo = Sub(10, 3)
	
	'''
	result = 0
	global relayDelay

	if (num1 - num2) < 0: result -= 256
	
	# 6502 8-bit subtraction program
	OP.CLC()							# Clear the carry flag
	time.sleep(relayDelay)
	RegB.write_inverted_register(num2)	# Input inverted num2 into register b
	time.sleep(relayDelay)
	temp = OP.ADC(0)					# ADd with Carry
	time.sleep(relayDelay)
	num2 = OP.STA()
	time.sleep(relayDelay)
	num2 = Add(num2, 1)					# Add 1 to the subtrahend to find the two's complement
	time.sleep(relayDelay)
	result += (Add(num1, num2)) & 0xFFFF
	time.sleep(relayDelay)
	
	return(result)
		
def Add(num1, num2):
	'''Returns the 8-bit addition of inputs
	
	Arguments:
		num1	8-bit integer
		num2	8-bit integer
		
	Returns:
		result	8-bit integer
		
	Example:
		foo = Add(10, 3)
	
	'''
	# 6502 8-bit addition program
	OP.CLC()				# clear the carry in
	time.sleep(relayDelay)
	OP.LDA(num1)			# load accumulator with num1
	time.sleep(relayDelay)
	cout = OP.ADC(num2)		# add num2 to accumulator
	time.sleep(relayDelay)
	result = OP.STA()		# store sum of num1 and num2
	time.sleep(relayDelay)

	return(result)
	
def Div(num1, num2):
	'''Returns the 8-bit division of inputs
	Relies on Sub function
	
	Arguments:
		num1	8-bit integer
		num2	8-bit integer
		
	Returns:
		result	8-bit integer
		
	Example:
		foo = Div(10, 3)
	
	'''
	# Set up variables
	count = 0
	adder = num2
	adder2 = 0
	adder3 = 0
	
	# Infinity test
	if num2 == 0:
		return(0)
	
	# Repeatedly increase num2, counting each time
	while adder <= num1:
		print("1")
		OP.CLC()
		adder3 = adder2
		adder2 = adder
		adder = Add(adder, num2)
		count += 1
		time.sleep(relayDelay)
		
		# This protects against the hw getting locked in a loop
		# due to the relays not switching. Return doesn't matter
		# due to hwResult = piResult assignment.
		if adder == adder2 and adder == adder3:
			return(0)
			
	return(count)		
	
def Mult(num1, num2):
	'''Returns the 8-bit multiplication of inputs
	Relies on Add function
	
	Arguments:
		num1	8-bit integer
		num2	8-bit integer
		
	Returns:
		result	8-bit integer
		
	Example:
		foo = Mult(10, 3)
	
	'''
	# Set up variables
	count = 0
	result = 0
	result2 = 0
	result3 = 0
	
	if num2 > num1:	# swap inputs for efficient computation
		num1 = num1 + num2
		num2 = num1 - num2
		num1 = num1 - num2
	for x in range(0, num2):
		result3 = result2
		result2 = result
		result = Add(result, num1)
		time.sleep(relayDelay)
		
		# This protects against the hw getting locked in a loop
		# due to the relays not switching. Return doesn't matter
		# due to hwResult = piResult assignment.
		if result == result2 and result == result3:
			return(0)
	return(result)
	
def AND(num1, num2):
	'''Returns the 8-bit bitwise AND of inputs
	
	Arguments:
		num1	8-bit integer
		num2	8-bit integer
		
	Returns:
		result	8-bit integer
		
	Example:
		foo = AND(0b11111100, 0b00010101)
	
	'''
	# 6502 8-bit AND program
	OP.CLC()
	OP.LDA(num1)
	OP.AND(num2)
	result = OP.STA()
	
	return(result)
	
def OR(num1, num2):
	'''Returns the 8-bit bitwise OR of inputs
	
	Arguments:
		num1	8-bit integer
		num2	8-bit integer
		
	Returns:
		result	8-bit integer
		
	Example:
		foo = OR(0b00001100,0b00000101)
	
	'''
	# 6502 8-bit OR program
	OP.CLC()
	OP.LDA(num1)
	OP.ORA(num2)
	result = OP.STA()
	
	return(result)
	
def XOR(num1, num2):
	'''Returns the 8-bit bitwise XOR of inputs
	
	Arguments:
		num1	8-bit integer
		num2	8-bit integer
		
	Returns:
		result	8-bit integer
		
	Example:
		foo = XOR(0b11011100, 0b00000101)
	
	'''
	# 6502 8-bit XOR program
	OP.CLC()
	OP.LDA(num1)
	OP.EOR(num2)
	result = OP.STA()
	
	return(result)
	
def Shift_r(num1, num_bits):
	'''Returns the 8-bit input, shifted right by num_bits binary places
	Note that this is not a rotation. I.e. zeroes are shifted in from
	the left.
	
	Arguments:
		num1		8-bit integer
		num_bits	8-bit integer
		
	Returns:
		result		8-bit integer
		
	Example:
		foo = Shift_r(0b11111001, 4)
	
	'''
	if num_bits > 8: # limits the number of cycles
		num_bits = 8
	for x in range(0, num_bits):
		# 6502 8-bit SR program
		OP.LDA(num1)
		OP.LSR()
		num1 = OP.STA()
		
	return(num1)
	
def Rotate_r(num1, num_bits):
	'''Returns the 8-bit input, rotated by num_bits binary places
	Current value of C_IN specifies value of MSB.
	
	Arguments:
		num1		8-bit integer
		num_bits	8-bit integer
		
	Returns:
		result		8-bit integer
		
	Example:
		foo = Rotate_r(0b11000000, 5)
	
	'''
	if num_bits > 8: # limits the number of cycles
		num_bits = 8		
	OP.SEC(num1 & 0b1)
	for x in range(0, num_bits):
		# 6502 8-bit ROR program
		OP.LDA(num1)
		OP.ROR()
		num1 = OP.STA()
		OP.SEC(ALU.read_C_OUT())
		
	return(num1, ALU.read_C_OUT())
