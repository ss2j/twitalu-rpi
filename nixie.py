#!/usr/bin/env python3

import sys
# appends to PYTHONPATH the location of the example codes
sys.path.append(r'/home/pi/git/quick2wire-python-api/')

import quick2wire.i2c as q2w_i2c
import twitalu_I2C as twi_i2c
import time
import math
import random

# Adder hold register display address block
d3_m1 = 0x08
d3_m2 = 0x09
d3_m3 = 0x0a
d3_m4 = 0x0b
d3_m5 = 0x0c

# Register A display address block
d1_m1 = 0x2B
d1_m2 = 0x2C
d1_m3 = 0x2D
d1_m4 = 0x2E
d1_m5 = 0x2F

# Register B display address block
d2_m1 = 0x30
d2_m2 = 0x31
d2_m3 = 0x32
d2_m4 = 0x33
d2_m5 = 0x35

# Module registers
char_reg = 0x00 	#[rw] 
addr_pins = 0x00 	#[rd]
bitmaph_reg = 0x01 	#[rw]
bitmapl_reg = 0x02 	#[rw]
sevenseg_reg = 0x03 #[rw]
dimmer_reg = 0x0B 	#[rw]
delay_reg = 0x0C 	#[rw]
command_reg = 0x0D 	#[rw]
fw_core = 0x0E 		#[rd]
fw_rev = 0x0F 		#[rd]

def init():
	"""
	Sets 75% brightness and blanks the displays.
	
	Accepts:
	--------
	* Nothing
	
	Returns:
	--------
	* Nothing
	"""
	
	blank_display()
	dim_display(0x75)

def dim_display(level, disp = 0):
	"""
	Sets the brightness of the displays.
	
	Accepts:
	--------
	* Brightness setpoint (0-100%)
	* Display [blank = all]
	
	Returns:
	--------
	* Nothing
	"""
	
	if(int(level) <= 0):
		new_lev = 0
	elif(int(level) >= 100):
		new_lev = 100
	else:
		new_lev = int(level)

	if(disp == 1):
		twi_i2c.write_data_gen(d1_m1, dimmer_reg, new_lev)	
		twi_i2c.write_data_gen(d1_m2, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d1_m3, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d1_m4, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d1_m5, dimmer_reg, new_lev)	

	elif(disp == 2):
		twi_i2c.write_data_gen(d2_m1, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d2_m2, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d2_m3, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d2_m4, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d2_m5, dimmer_reg, new_lev)
			
	elif(disp == 3):
		twi_i2c.write_data_gen(d3_m1, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d3_m2, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d3_m3, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d3_m4, dimmer_reg, new_lev)
		twi_i2c.write_data_gen(d3_m5, dimmer_reg, new_lev)
	else:
		dim_display(level, 1)
		dim_display(level, 2)
		dim_display(level, 3)

def blank_display(disp = 0):
	"""
	Turns displays off.
	
	Accepts:
	--------
	* Display [blank = all]
	
	Returns:
	--------
	* Nothing
	"""
	
	if(disp == 1):
		# Make sure the burn in routine is stopped as it overrides the blank
		twi_i2c.write_data_gen(d1_m1, command_reg, 0x02)
		twi_i2c.write_data_gen(d1_m2, command_reg, 0x02)
		twi_i2c.write_data_gen(d1_m3, command_reg, 0x02)
		twi_i2c.write_data_gen(d1_m4, command_reg, 0x02)
		twi_i2c.write_data_gen(d1_m5, command_reg, 0x02)
		# Begin blanking the displays
		twi_i2c.write_data_gen(d1_m1, char_reg, 0b00010000)
		twi_i2c.write_data_gen(d1_m5, char_reg, 0b00010000)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d1_m2, char_reg, 0b00010000)
		twi_i2c.write_data_gen(d1_m4, char_reg, 0b00010000)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d1_m3, char_reg, 0b00010000)
		
	elif(disp == 2):
		# Disbale the burn in routine as it overrides blanking command
		twi_i2c.write_data_gen(d2_m1, command_reg, 0x02)
		twi_i2c.write_data_gen(d2_m2, command_reg, 0x02)
		twi_i2c.write_data_gen(d2_m3, command_reg, 0x02)
		twi_i2c.write_data_gen(d2_m4, command_reg, 0x02)
		twi_i2c.write_data_gen(d2_m5, command_reg, 0x02)
		# Beging blanking displays
		twi_i2c.write_data_gen(d2_m1, char_reg, 0b00010000)
		twi_i2c.write_data_gen(d2_m5, char_reg, 0b00010000)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d2_m2, char_reg, 0b00010000)
		twi_i2c.write_data_gen(d2_m4, char_reg, 0b00010000)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d2_m3, char_reg, 0b00010000)
		
	elif(disp == 3):
		# Disable the burn in routine as it overrides the blanking command
		twi_i2c.write_data_gen(d3_m1, command_reg, 0x02)
		twi_i2c.write_data_gen(d3_m2, command_reg, 0x02)
		twi_i2c.write_data_gen(d3_m3, command_reg, 0x02)
		twi_i2c.write_data_gen(d3_m4, command_reg, 0x02)
		twi_i2c.write_data_gen(d3_m5, command_reg, 0x02)
		# Begin blanking the displays
		twi_i2c.write_data_gen(d3_m1, char_reg, 0b00010000)
		twi_i2c.write_data_gen(d3_m5, char_reg, 0b00010000)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d3_m2, char_reg, 0b00010000)
		twi_i2c.write_data_gen(d3_m4, char_reg, 0b00010000)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d3_m3, char_reg, 0b00010000)
		
	else:
		blank_display(1)
		blank_display(2)
		blank_display(3)

def module_info(disp = 0):
	"""
	Reads the f/w version from modules in a display.
	
	Accepts:
	--------
	* Display [blank = all]
	
	Returns:
	--------
	* Nothing
	"""
	
	if(disp == 1):
		print("|_ Reading FW versions")
		core = twi_i2c.read_data_gen(d1_m1, fw_core)
		rev = twi_i2c.read_data_gen(d1_m1, fw_rev)
		twi_i2c.write_data_gen(d1_m1, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d1_m1, addr_pins)
		print("|__ Module 1")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d1_m2, fw_core)
		rev = twi_i2c.read_data_gen(d1_m2, fw_rev)
		twi_i2c.write_data_gen(d1_m2, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d1_m2, addr_pins)
		print("|__ Module 2")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d1_m3, fw_core)
		rev = twi_i2c.read_data_gen(d1_m3, fw_rev)
		twi_i2c.write_data_gen(d1_m3, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d1_m3, addr_pins)
		print("|__ Module 3")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d1_m4, fw_core)
		rev = twi_i2c.read_data_gen(d1_m4, fw_rev)
		twi_i2c.write_data_gen(d1_m4, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d1_m4, addr_pins)
		print("|__ Module 4")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d1_m5, fw_core)
		rev = twi_i2c.read_data_gen(d1_m5, fw_rev)
		twi_i2c.write_data_gen(d1_m5, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d1_m5, addr_pins)
		print("|__ Module 5")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
	elif(disp == 2):
		print("|_ Reading FW versions")
		core = twi_i2c.read_data_gen(d2_m1, fw_core)
		rev = twi_i2c.read_data_gen(d2_m1, fw_rev)
		twi_i2c.write_data_gen(d2_m1, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d2_m1, addr_pins)
		print("|__ Module 1")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d2_m2, fw_core)
		rev = twi_i2c.read_data_gen(d2_m2, fw_rev)
		twi_i2c.write_data_gen(d2_m2, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d2_m2, addr_pins)
		print("|__ Module 2")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d2_m3, fw_core)
		rev = twi_i2c.read_data_gen(d2_m3, fw_rev)
		twi_i2c.write_data_gen(d2_m3, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d2_m3, addr_pins)
		print("|__ Module 3")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d2_m4, fw_core)
		rev = twi_i2c.read_data_gen(d2_m4, fw_rev)
		twi_i2c.write_data_gen(d2_m4, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d2_m4, addr_pins)
		print("|__ Module 4")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d2_m5, fw_core)
		rev = twi_i2c.read_data_gen(d2_m5, fw_rev)
		twi_i2c.write_data_gen(d2_m5, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d2_m5, addr_pins)
		print("|__ Module 5")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
	elif(disp == 3):
		print("|_ Reading FW versions")
		core = twi_i2c.read_data_gen(d3_m1, fw_core)
		rev = twi_i2c.read_data_gen(d3_m1, fw_rev)
		twi_i2c.write_data_gen(d3_m1, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d3_m1, addr_pins)
		print("|__ Module 1")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d3_m2, fw_core)
		rev = twi_i2c.read_data_gen(d3_m2, fw_rev)
		twi_i2c.write_data_gen(d3_m2, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d3_m2, addr_pins)
		print("|__ Module 2")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d3_m3, fw_core)
		rev = twi_i2c.read_data_gen(d3_m3, fw_rev)
		twi_i2c.write_data_gen(d3_m3, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d3_m3, addr_pins)
		print("|__ Module 3")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d3_m4, fw_core)
		rev = twi_i2c.read_data_gen(d3_m4, fw_rev)
		twi_i2c.write_data_gen(d3_m4, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d3_m4, addr_pins)
		print("|__ Module 4")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
		core = twi_i2c.read_data_gen(d3_m5, fw_core)
		rev = twi_i2c.read_data_gen(d3_m5, fw_rev)
		twi_i2c.write_data_gen(d3_m5, command_reg, 0xF0)
		addr = twi_i2c.read_data_gen(d3_m5, addr_pins)
		print("|__ Module 5")
		print("|___ Core: {0} Version:{1} Address:{2}".format(core, rev, addr))
		
	else:
		print("nixie.module_info says: no argument passed chief...")

def write_value(value, disp = 0):
	"""
	Writes an integer value to a display. Time per digit is random.
	
	Accepts:
	--------
	* Value to display [Min value: 00000 Max value: 99999]
	* Display [blank = all]
	
	Returns:
	--------
	* Nothing
	"""
	
	if(value > 99999):
		print("nixie.write_value value out of range [++]")
	if(value < -99999):
		print("nixie.write_value value out of range [--]")
		
	temp = math.fabs(value)
		
	if(disp == 1):
		m5 = math.floor(temp / 10000)
		twi_i2c.write_data_gen(d1_m5, command_reg, 0x02)
		if(value < 0):
			twi_i2c.write_data_gen(d1_m5, char_reg, (0b10000000 | m5))	# Turn on the minus
		else:
			twi_i2c.write_data_gen(d1_m5, char_reg, m5)
		time.sleep(random.random())
		
		m4 = math.floor( (temp - (m5*10000)) / 1000)
		twi_i2c.write_data_gen(d1_m4, command_reg, 0x02)
		if(m4 > 0):
			twi_i2c.write_data_gen(d1_m4, char_reg, (m4 | 0b01000000))
		else:
			twi_i2c.write_data_gen(d1_m4, char_reg, m4)
		twi_i2c.write_data_gen(d1_m4, char_reg, m4)
		time.sleep(random.random())
		
		m3 = math.floor( (temp - (m4*1000) - (m5*10000)) / 100)
		twi_i2c.write_data_gen(d1_m3, command_reg, 0x02)
		twi_i2c.write_data_gen(d1_m3, char_reg, m3)
		time.sleep(random.random())
		
		m2 = math.floor( (temp - (m3*100) - (m4*1000) - (m5*10000)) / 10)
		twi_i2c.write_data_gen(d1_m2, command_reg, 0x02)
		twi_i2c.write_data_gen(d1_m2, char_reg, m2)
		time.sleep(random.random())
		
		m1 = math.floor( (temp - (m2*10) - (m3*100) - (m4*1000) - (m5*10000)) / 1)
		twi_i2c.write_data_gen(d1_m1, command_reg, 0x02)
		twi_i2c.write_data_gen(d1_m1, char_reg, m1)

	elif(disp == 2):
		m5 = math.floor(temp / 10000)
		twi_i2c.write_data_gen(d2_m5, command_reg, 0x02)
		if(value < 0):
			twi_i2c.write_data_gen(d2_m5, char_reg, (0b10000000 | m5))	# Turn on the minus
		else:
			twi_i2c.write_data_gen(d2_m5, char_reg, m5)
		time.sleep(random.random())
		
		m4 = math.floor( (temp - (m5*10000)) / 1000)
		twi_i2c.write_data_gen(d2_m4, command_reg, 0x02)
		if(m4 > 0):
			twi_i2c.write_data_gen(d2_m4, char_reg, (m4 | 0b01000000))
		else:
			twi_i2c.write_data_gen(d2_m4, char_reg, m4)
		twi_i2c.write_data_gen(d2_m4, char_reg, m4)
		time.sleep(random.random())
		
		m3 = math.floor( (temp - (m4*1000) - (m5*10000)) / 100)
		twi_i2c.write_data_gen(d2_m3, command_reg, 0x02)
		twi_i2c.write_data_gen(d2_m3, char_reg, m3)
		time.sleep(random.random())
		
		m2 = math.floor( (temp - (m3*100) - (m4*1000) - (m5*10000)) / 10)
		twi_i2c.write_data_gen(d2_m2, command_reg, 0x02)
		twi_i2c.write_data_gen(d2_m2, char_reg, m2)
		time.sleep(random.random())
		
		m1 = math.floor( (temp - (m2*10) - (m3*100) - (m4*1000) - (m5*10000)) / 1)
		twi_i2c.write_data_gen(d2_m1, command_reg, 0x02)
		twi_i2c.write_data_gen(d2_m1, char_reg, m1)
		
	elif(disp == 3):
		m5 = math.floor(temp / 10000)
		twi_i2c.write_data_gen(d3_m5, command_reg, 0x02)
		if(value < 0):
			twi_i2c.write_data_gen(d3_m5, char_reg, (0b10000000 | m5))	# Turn on the minus
		else:
			twi_i2c.write_data_gen(d3_m5, char_reg, m5)
		time.sleep(random.random())
		
		m4 = math.floor( (temp - (m5*10000)) / 1000)
		twi_i2c.write_data_gen(d3_m4, command_reg, 0x02)
		if(m4 > 0):
			twi_i2c.write_data_gen(d3_m4, char_reg, (m4 | 0b01000000))
		else:
			twi_i2c.write_data_gen(d3_m4, char_reg, m4)
		twi_i2c.write_data_gen(d3_m4, char_reg, m4)
		time.sleep(random.random())
		
		m3 = math.floor( (temp - (m4*1000) - (m5*10000)) / 100)
		twi_i2c.write_data_gen(d3_m3, command_reg, 0x02)
		twi_i2c.write_data_gen(d3_m3, char_reg, m3)
		time.sleep(random.random())
		
		m2 = math.floor( (temp - (m3*100) - (m4*1000) - (m5*10000)) / 10)
		twi_i2c.write_data_gen(d3_m2, command_reg, 0x02)
		twi_i2c.write_data_gen(d3_m2, char_reg, m2)
		time.sleep(random.random())
		
		m1 = math.floor( (temp - (m2*10) - (m3*100) - (m4*1000) - (m5*10000)) / 1)
		twi_i2c.write_data_gen(d3_m1, command_reg, 0x02)
		twi_i2c.write_data_gen(d3_m1, char_reg, m1)
		
	else:
		print("nixie.write_value says: no argument passed chief...")

def tumble_display(disp = 0):
	"""
	Starts display tumble effect. Tumble rate per digit is random.
	
	Accepts:
	--------
	* Display [blank = all]
	
	Returns:
	--------
	* Nothing
	"""
	
	if(disp == 1):
		twi_i2c.write_data_gen(d1_m1, delay_reg, int( random_value() ))	#sets the count rate
		twi_i2c.write_data_gen(d1_m1, command_reg, 0x01)	#starts the burn in routine
		twi_i2c.write_data_gen(d1_m5, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d1_m5, command_reg, 0x01)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d1_m2, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d1_m2, command_reg, 0x01)
		twi_i2c.write_data_gen(d1_m4, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d1_m4, command_reg, 0x01)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d1_m3, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d1_m3, command_reg, 0x01)
		
	elif(disp == 2):
		twi_i2c.write_data_gen(d2_m1, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d2_m1, command_reg, 0x01)
		twi_i2c.write_data_gen(d2_m5, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d2_m5, command_reg, 0x01)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d2_m2, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d2_m2, command_reg, 0x01)
		twi_i2c.write_data_gen(d2_m4, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d2_m4, command_reg, 0x01)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d2_m3, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d2_m3, command_reg, 0x01)
		
	elif(disp == 3):
		twi_i2c.write_data_gen(d3_m1, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d3_m1, command_reg, 0x01)
		twi_i2c.write_data_gen(d3_m5, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d3_m5, command_reg, 0x01)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d3_m2, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d3_m2, command_reg, 0x01)
		twi_i2c.write_data_gen(d3_m4, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d3_m4, command_reg, 0x01)
		time.sleep(0.05)
		twi_i2c.write_data_gen(d3_m3, delay_reg, int( random_value() ))
		twi_i2c.write_data_gen(d3_m3, command_reg, 0x01)
		
	else:
		tumble_display(1)
		tumble_display(2)
		tumble_display(3)

def random_value():
	"""
	Provides a random value for use in delaying displays.
	
	Accepts:
	--------
	* Nothing
	
	Returns:
	--------
	* Random value [Min value: 3 Max value: 10]
	"""
	
	temp = ( random.random() * 10 )
	if temp <= 3:
		return 3
	elif temp >= 10:
		return 10
	else:
		return temp
